# irstea/phploc-shim - shim repository for phploc/phploc.

This package is a drop-in replacement for [phploc/phploc](https://github.com/sebastianbergmann/phploc), which provides its PHAR archive as a binary.

It is built automatically from the official PHAR.

## Installation

	composer require irstea/phploc-shim

or:

	composer require --dev irstea/phploc-shim



## Usage

As you would use the original package, i.e. something like:

	vendor/bin/phploc [options] [arguments]

## License

This distribution retains the license of the original software: BSD-3-Clause